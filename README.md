Morse
=====

Ansible base role for deploying Docker Compose based services.

Description
-----------

Morse is a Ansible base role which can be used to install, update or remove
Docker Compose templates.

By default, Morse installs each service into a subdirectory of the `/srv/`
directory on the target host.

Morse keeps track of previous releases and can perform in-place upgrades.
Files managed by Morse are removed or replaced during upgrades, while
unmanaged files (e.g., data directories) are kept in place. Morse can also
install a Systemd unit file for the service, and manage the stop/start
cycle during upgrades.

Morse is a *base role*, which means you need to define your own role which
calls Morse. Calling Morse directly has no effect.

### Directory Structure Example

For a service named `postgres`:

```
/srv/
  postgres/
    .env  -> .releases/current/.env
    docker-compose.yml  -> .releases/current/docker-compose.yml
    conf  -> .releases/current/conf
    data/

    .releases/
      20210210T100000Z/
        .env
        docker-compose.yml
        conf/
          postgresql.conf
      current  -> 20210210T100000Z
```


Requirements
------------

Docker and Docker Compose must be installed and running on the target host.

Currently, Morse only works with operating systems which use Systemd.


Usage Guide
-----------

Do not include Morse directly, instead call it from another role. See below for
a minimal example.

Content in the subrole's `files/` and `templates/` directories, as well as any
callback hooks defined (see below) are used to install the Docker Compose
service.

### Service Directory Structure

### Hooks

At several points during the installation procedure, optional callbacks can be
provided which can contain an additional list of tasks for Ansible to execute at
that point. Hooks are placed in the `tasks/` directory of the subrole. If a hook
file is not present, it will be skipped.

| Hook                | Description                                         |
|---------------------|-----------------------------------------------------| 
| `pre-install.yml`   | [`present`] New install: runs after files have been copied, but before service has started
| `post-install.yml`  | [`present`] New install: runs after service has started
| `pre-upgrade.yml`   | [`present`] Existing install: runs after files have been copied, but before service has started
| `post-upgrade.yml`  | [`present`] Existing install: runs after service has started
| `pre-remove.yml`    | [`absent`] Runs before stopping the service
| `post-remove.yml`   | [`absent`] Runs after the service has stopped



Role Variables
--------------

The following parameters can be passed to the role.

> 💡 **Tip**  
> It is recommended to use the `vars` attribute on the `include_role` task for
> passing these arguments rather than through inventory.

&nbsp;

---

`name`        &emsp; [default: Last component of parent role name]
> Name of the service to install. Will be used in the service directory name and
> for Systemd.

---

`description` &emsp; [default: `name | title`]               
> Description of service used in Systemd unit file. 

---

`state`       &emsp; [default: **present**]
> * **present**: Service will be installed if not already installed, or
>   otherwise upgraded.
> * **absent**: Service will be uninstalled

---

`_docker_compose_exe` &emsp; [default: **/usr/bin/docker-compose**]
> Path to the **docker-compose** command.

---

`_morse_src` &emsp; [default: Path of parent role]
> Path relative to which files, templates and callbacks are found. Usually, this
> will be the path to the invoking subrole.

---

`_morse_src_files` &emsp; [default: `_morse_src`**/files**]
> Plain files which will be copied to the service directory.

---

`_morse_src_templates` &emsp; [default: `_morse_src`**/templates**]
> Templated files which will be rendered and copied to the service directory.
> Templated files are copied after plain files. The extension `.j2` is stripped
> from the file names.

---

`_morse_env` &emsp; [default: **{}**]
> A list of key-value pairs which will be placed in the `.env` file in the root
> of the service directory.

---

`_morse_pull_images` &emsp; [default: **true**]
> Set to false to skip pulling images (useful if the images are already present 
> on the host).

---

`_morse_service_parent_dir` &emsp; [default: **/srv**]  
> Base directory in which all Morse services will be installed.

---

`_morse_service_dir` &emsp; [default: `_morse_service_parent_dir`**/**`name`]  
> Directory in which this Morse service will be installed.

---

`_morse_system_service` &emsp; [default: **true**]
> Set to true to have Morse install a Systemd service and manage
> stopping/starting the service. Set to false, and Morse will only install files
> to the service directory, but not manage the service itself.
>
> Useful for deploying a Morse service locally for development purposes.

---

`_morse_system_service_wait` &emsp; [default: **5**]
> How many seconds to wait before testing if docker-compose is running and
> has not yet crashed.

---

`_morse_prune_keep` &emsp; [default: **5**]
> How many old revisions to keep.



Minimal Example Subrole
-----------------------

**roles/srv/alpine/tasks/main.yml**:
```yaml
- include_role:
    name: morse
  vars:
    _morse_env:
      ALPINE_VERSION: latest
```

**roles/srv/alpine/files/docker-compose.yml**:
```yaml
version: '3'
services:
  alpine:
    image: alpine:${ALPINE_VERSION}
```

**playbooks/srv_alpine.yml**:
```yaml
- hosts: all
  roles:
    - srv/alpine
```

Example Inventory for Local Development
---------------------------------------

**inventory/local/hosts**:
```ini
localhost

[localhost]
_morse_service_parent_dir=/tmp/morse
_morse_system_service=false
```


License
-------

MIT

To Do
-----

- [ ] Automated rollback on failure
- [ ] Support for other init systems beside Systemd
- [ ] Diff file trees and only restart service if required

<!-- vim: set nowrap -->
